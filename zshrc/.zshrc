# Configuring Zsh
autoload -Uz compinit promptinit
compinit
promptinit
zstyle ':completion:*' menu select
setopt COMPLETE_ALIASES

# Plugins
source ~/.aliases
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
source /usr/share/zsh-theme-powerlevel9k/powerlevel9k.zsh-theme

# Environment Variables
export EDITOR='vim'
export PATH="${PATH}:/home/th3lusive/.config/scripts"
export PATH="${PATH}:/home/th3lusive/.local/bin"
export PATH="$HOME/.node_modules/bin:$PATH"
export npm_config_prefix=~/.node_modules

# Appearance (Powerlevel9k)
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(root_indicator host dir vcs status)
POWERLEVEL9K_PROMPT_ON_NEWLINE=0
POWERLEVEL9K_STATUS_VERBOSE=false
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=()
export DEFAULT_USER="$USER"

POWERLEVEL9K_HOME_ICON=''

# FASD
alias a='fasd -a'        # any
alias s='fasd -si'       # show / search / select
alias d='fasd -d'        # directory
alias f='fasd -f'        # file
alias sd='fasd -sid'     # interactive directory selection
alias sf='fasd -sif'     # interactive file selection
alias z='fasd_cd -d'     # cd, same functionality as j in autojump
alias zz='fasd_cd -d -i' # cd with interactive selection

alias v='f -e vim' # quick opening files with vim
alias m='f -e mpv' # quick opening files with mplayer
alias o='a -e xdg-open' # quick opening files with xdg-open

# Msc
eval "$(fasd --init auto)"
source ~/.nnnrc
(cat ~/.cache/wal/sequences &)
