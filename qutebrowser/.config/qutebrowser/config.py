	#l from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
	# from qutebrowser.config.config import ConfigContainer  # noqa: F401
	# config = config  # type: ConfigAPI # noqa: F821 pylint: disable=E0602,C0103
	# c = c  # type: ConfigContainer # noqa: F821 pylint: disable=E0602,C0103
	# from enum import enum	
# Themes

# Global settings
c.auto_save.session = True
c.completion.scrollbar.width = 8
c.scrolling.smooth = True
c.downloads.position = 'top'
c.downloads.remove_finished = 8000
c.fonts.monospace = 'Hack Regular'
c.fonts.tabs = '10pt Roboto Regular'
c.hints.auto_follow_timeout = 500
c.hints.border = '1px solid #ffa500'
c.hints.scatter = False
c.input.insert_mode.auto_leave = False
c.input.insert_mode.auto_load = False
c.input.insert_mode.plugins = True
c.completion.height = '30%'
c.statusbar.hide = False
c.statusbar.padding = {'top': 1, 'bottom': 1, 'left': 8, 'right': 8}
c.messages.timeout = 8000
c.confirm_quit = ["multiple-tabs"]
c.content.pdfjs = True

c.downloads.location.directory = '~/Downloads'

c.url.searchengines = { "DEFAULT": 'https://duckduckgo.com/?q={}' ,  "aw": 'https://wiki.archlinux.org/?search={}', "r": 'https://www.reddit.com/r/{}', "pocket": 'https://getpocket.com/a/queue/{}', "gw": 'https://wiki.gentoo.org/?search={}'}

# Window Title

# Tab Settings
c.tabs.last_close = 'close'
c.tabs.background = True
c.tabs.favicons.show = True
c.tabs.favicons.scale = 1.2 
c.tabs.indicator.padding = {'top': 0, 'bottom': 0, 'left': 4, 'right': 4}
c.tabs.padding = {'top': 2, 'bottom': 2, 'left': 8, 'right': 8}
c.tabs.position = 'top'
c.tabs.show = 'always'
c.tabs.title.alignment = 'center'
c.tabs.show_switching_delay = 1000
c.tabs.title.format = '[{index}] {private}{title}'
c.tabs.title.format_pinned = '{index}'

# completion
c.colors.completion.category.bg = '#232323'
c.colors.completion.category.border.bottom = '#f9f5f0'
c.colors.completion.category.border.top = '#f9f5f0'
c.colors.completion.category.fg = '#c7c4c0'
c.colors.completion.even.bg = '#333333'
c.colors.completion.fg = '#c7c4c0'
c.colors.completion.item.selected.bg = '#789048'
c.colors.completion.item.selected.border.bottom = '#232323'
c.colors.completion.item.selected.border.top = '#333333'
c.colors.completion.item.selected.fg = '#232323'
c.colors.completion.match.fg = '#ffa500'
c.colors.completion.odd.bg = '#232323'
c.colors.completion.scrollbar.bg = '#232323'
c.colors.completion.scrollbar.fg = '#464646'

# downloads
c.colors.downloads.bar.bg = '#232323'
c.colors.downloads.start.bg = '#232323'
c.colors.downloads.start.fg = '#f9f5f0'
c.colors.downloads.stop.bg = '#789048'
c.colors.downloads.stop.fg = '#232323'

# hints
c.colors.hints.bg = '#333333'
c.colors.hints.fg = '#c7c4c0'
c.colors.hints.match.fg = '#ffa500'
c.hints.uppercase = True
c.hints.chars = 'asdfghjklqwertyzxcvb'

# messages
c.colors.messages.error.bg = '#aa4b66'
c.colors.messages.error.fg = '#232323'
c.colors.messages.info.bg = '#232323'
c.colors.messages.info.fg = '#464646'
c.colors.messages.warning.bg = '#232323'
c.colors.messages.warning.fg = '#f9f5f0'

# prompt
c.prompt.filebrowser = True
c.prompt.radius = 0
c.colors.prompts.bg = '#232323'
c.colors.prompts.fg = '#f9f5f0'
c.colors.prompts.selected.bg = '#789048'

# statusbar
c.colors.statusbar.command.bg = '#5665aa'
c.colors.statusbar.command.fg = '#232323'
c.colors.statusbar.insert.bg = '#789048'
c.colors.statusbar.insert.fg = '#232323'
c.colors.statusbar.normal.bg = '#232323'
c.colors.statusbar.normal.fg = '#c7c4c0'
c.colors.statusbar.progress.bg = '#f9f5f0'
c.colors.statusbar.url.success.http.fg = '#789048'
c.colors.statusbar.url.success.https.fg = '#789048'

# tabs
c.colors.tabs.even.bg = '#373b41'
c.colors.tabs.even.fg = '#c7c4c0'
c.colors.tabs.odd.bg = '#232323'
c.colors.tabs.odd.fg = '#c7c4c0'
c.colors.tabs.selected.even.bg = '#789048'
c.colors.tabs.selected.even.fg = '#232323'
c.colors.tabs.selected.odd.bg = '#789048'
c.colors.tabs.selected.odd.fg = '#232323'

# aliases
c.aliases = {
   'pocket': 'open -t https://getpocket.com/edit?url={url}',
}


# Bindings for normal mode
config.bind('M', 'hint links spawn mpv {hint-url}')
config.bind('m', 'spawn mpv {url}')
config.bind('B', 'spawn buku -a {url}')
config.bind('zp', 'spawn open -t https://getpocket.com/edit?url={url}')

config.bind('J', 'tab-prev')
config.bind('K', 'tab-next')
config.bind('q', 'quit --save')
config.bind('Q', 'quit')
config.source('shortcuts.py')
