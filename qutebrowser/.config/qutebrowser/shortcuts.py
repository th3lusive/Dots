# qutebrowser shortcuts
config.bind(';d', 'set downloads.location.directory ~/Documents ;; hint links download')
config.bind(';D', 'set downloads.location.directory ~/Downloads ;; hint links download')
config.bind(';m', 'set downloads.location.directory ~/Music ;; hint links download')
config.bind(';p', 'set downloads.location.directory ~/Pictures ;; hint links download')
config.bind(';W', 'set downloads.location.directory ~/Pictures/Wallpapers ;; hint links download')
config.bind(';v', 'set downloads.location.directory ~/Videos ;; hint links download')
config.bind(';s', 'set downloads.location.directory ~/.config/scripts ;; hint links download')
config.bind(';cf', 'set downloads.location.directory ~/.config ;; hint links download')
